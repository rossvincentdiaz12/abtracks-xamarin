# ABTRACKS Android Xamarin

Xamarin Android source code of ABTRACKS

## Getting Started
 git clone https://gitlab.com/rossvincentdiaz12/abtracks-xamarin.git
 
 open the project on visual studio code 2015 and above

### Prerequisites

Set up you firebase database

* [Firebase](https://console.firebase.google.com)

### Installing

install

* firebase database
* firebase auth
* firebase database connection

## Deployment

open you terminal and run the code

sudo python3 GPS_API.py

## Built With

* [Xamarin](https://dotnet.microsoft.com/apps/xamarin) 
* [Firebase](https://console.firebase.google.com)


## Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details